package pasarela.pagos.Pagos.Tigo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pasarela.pagos.Pagos.Tigo.model.Persona;

@Repository
public interface PersonasRepository extends JpaRepository<Persona, Integer> {

}
