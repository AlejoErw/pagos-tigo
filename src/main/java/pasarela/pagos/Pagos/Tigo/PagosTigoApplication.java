package pasarela.pagos.Pagos.Tigo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PagosTigoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PagosTigoApplication.class, args);
	}

}
