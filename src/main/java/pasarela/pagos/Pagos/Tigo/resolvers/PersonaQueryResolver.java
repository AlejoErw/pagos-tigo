package pasarela.pagos.Pagos.Tigo.resolvers;

import java.util.List;
import java.util.Optional;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import graphql.GraphQL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import pasarela.pagos.Pagos.Tigo.model.Persona;
import pasarela.pagos.Pagos.Tigo.service.PersonaService;

@Component
public class PersonaQueryResolver implements GraphQLQueryResolver {

	@Autowired
	private PersonaService personaSvc;
	
	public List<Persona> personas(final int cantidad) {
		return personaSvc.personas(cantidad);
	}
	
	public Optional<Persona> persona(final int id) {
		return personaSvc.persona(id);
	}
}

