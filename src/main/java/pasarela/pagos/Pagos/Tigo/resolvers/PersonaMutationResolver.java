package pasarela.pagos.Pagos.Tigo.resolvers;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import pasarela.pagos.Pagos.Tigo.model.Persona;
import pasarela.pagos.Pagos.Tigo.service.PersonaService;

import javax.persistence.criteria.CriteriaBuilder;

@Component
public class PersonaMutationResolver implements GraphQLMutationResolver {

	@Autowired
	private PersonaService personaSvc;
	
	public Persona registrarPersona(final String nombre, String correo,
									final String apellidoPaterno, final String apellidoMaterno, final String fechaNacimiento) {
		return personaSvc.registrarPersona(nombre, correo, apellidoPaterno, apellidoMaterno, fechaNacimiento);
	}

//	public Persona eliminarPersona(final Integer id) {
//		return personaSvc.eliminarPersona(id);
//	}



}



